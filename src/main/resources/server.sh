#!/bin/sh

RETVAL=0
prog="MYJSVC"

#应用程序的目录
MYJSVC_HOME=/opt/${project.name}-${project.version}
#jsvc所在的目录
DAEMON_HOME=/opt/jsvc/unix
#用户
MYJSVC_USER=root

# for multi instances adapt those lines.
TMP_DIR=$MYJSVC_HOME/tmp
PID_FILE=$MYJSVC_HOME/tlstat.pid

#程序运行是所需的jar包，commons-daemon.jar是不能少的
CLASSPATH=\
/opt/jsvc/commons-daemon.jar
for file in $MYJSVC_HOME/lib/*.jar
do
 CLASSPATH="$CLASSPATH:$file"
done


case "$1" in
start)
#
# Start TlStat Data Serivce
#
$DAEMON_HOME/jsvc -user $MYJSVC_USER -home $JAVA_HOME -Djava.io.tmpdir=$TMP_DIR -wait 10 -pidfile $PID_FILE -outfile $MYJSVC_HOME/log/myjsvc.out -errfile '&1' -cp $CLASSPATH cn.buglife.demo.Application

#
# To get a verbose JVM
#-verbose \
# To get a debug of jsvc.
#-debug \
exit $?
;;

stop)
#
# Stop TlStat Data Serivce
#
$DAEMON_HOME/jsvc -stop -pidfile $PID_FILE cn.buglife.demo.Application
exit $?
;;

*)
echo "Usage myjsvc start/stop"
exit 1;;
esac