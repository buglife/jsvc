package cn.buglife.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by zhangjun on 16/3/20.
 */
@ComponentScan(value = "cn.buglife.demo")
@EnableAutoConfiguration
@SpringBootApplication
@Configuration
@EnableScheduling
@EnableConfigurationProperties
public class Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
    
    public static void main(String[] args) {
        SpringApplication.run(Application.class,args);
    }

    public void init() throws Exception {
        LOGGER.info("execute init method！");
    }

    public void init(String[] args) throws Exception{
        LOGGER.info("execute init(args) method");
    }

    public void start() throws Exception {
        SpringApplication.run(Application.class);
    }

    public void stop() throws Exception {
        LOGGER.info("execute stop method！");
    }

    public void destroy() throws Exception{
        LOGGER.info("execute destroy method!");
    }
}
