package cn.buglife.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

/**
 * Created by zhangjun on 16/3/20.
 */
@Component
public class RemindJob {


    private static final Logger LOGGER = LoggerFactory.getLogger(RemindJob.class);

    @Value(value = "${spring.mail.from}")
    private String from;

    @Autowired
    private JavaMailSender javaMailSender;

    @Scheduled(cron = "0/20 * * * * ?")
    public void sendEmail(){

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(from);
            messageHelper.setTo("zhangjun@buglife.cn");
            messageHelper.setSubject("启动java服务测试");
            mimeMessage.setText("JSVC制作Linux服务");
        } catch (MessagingException e) {
            LOGGER.error(e.getMessage());
        }

        javaMailSender.send(mimeMessage);
    }
}
