## JSVC简介
 > Jsvc is a set of libraries and applications for making Java applications run on UNIX more easily.

## 安装JSVC
1. 从tomcat的bin目录下获取commons-daemon-native.tar.gz压缩包
2. 解压，会发现如下目录结构
<![JSVC目录结构](http://image.buglife.cn/blog/2.pic.jpg)
3. 显而易见以上的目录结构中区分了windows和Linux系统不同的使用方式，在此我们仅演示Linux，即针对unix目录里内容的讲解。打开unix目录并执行

  ```
    sh support/buildconf.sh

  ```
4. 进行编译安装
  > 在安装之前确保本地安装了jdk并设置了JAVA_HOME环境变量

  ```
    ./configure

    make
  ```

## JAVA项目实战
[源码参考](https://git.oschina.net/buglife/jsvc)
